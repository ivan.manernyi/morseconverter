﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MorseConverter
{
    public class Program
    {
        public static IDictionary<string, string> ConverterDict; // Словарь для конвертации

        public static void Main(string[] args)
        {
            do
            {
                Console.WriteLine("Введите код Морзе для расшифровки:");
                string inputText = Console.ReadLine();
                GetInputText(inputText);
            }   while (true);
        }

        private static void LoadDictionary()
        {
            ConverterDict = new Dictionary<string, string>() {
                { ".-", "A"},
                { "-...", "B"},
                { "-.-.", "C"},
                { "-..", "D"},
                { ".", "E"},
                { "..-.", "F"},
                { "--.", "G"},
                { "....", "H"},
                { "..", "I"},
                { ".---", "J"},
                { "-.-", "K"},
                { ".-..", "L"},
                { "--", "M"},
                { "-.", "N"},
                { "---", "O"},
                { ".--.", "P"},
                { "--.-", "Q"},
                { ".-.", "R"},
                { "...", "S"},
                { "-", "T"},
                { "..-", "U"},
                { "...-", "V"},
                { ".--", "W"},
                { "-..-", "X"},
                { "-.--", "Y"},
                { "--..", "Z"},
            };
        }

        public static string GetInputText(string inputText)
        {
            LoadDictionary(); // Создаем словарь
            if (!string.IsNullOrEmpty(inputText) && !string.IsNullOrWhiteSpace(inputText))
            {
                inputText = inputText.ToLower();
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Расшифровка: ");
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                string returnText = ConvertToText(inputText);
                Console.WriteLine(returnText);
                Console.ResetColor();
                return returnText;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Ошибка! Введена пустая или состоящая из пробелов строка!");
                Console.ResetColor();
                return "NullOrEmptyStringError";
            }
        }

        private static string ConvertToText(string inputText)
        {
            StringBuilder stringBuilder = new StringBuilder(); // Инициализация стрингбилдера
            string[] wordsArr = new string[] { "" }; // Массив слов
            List<string> convertedWords = new List<string>(); // Лист для хранения расшифрованных слов
            wordsArr = inputText.Split("   "); // Получаем список слов
            string[] charArr; // Массив символов в каждом слове
            foreach (var word in wordsArr)
            {
                charArr = word.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries); // Получаем список символов каждого слова
                foreach (var character in charArr)
                {
                    if (ConverterDict.ContainsKey(character)) // Если символ найден в словаре
                    {
                        stringBuilder.Append(ConverterDict[character]);
                    }
                    else // Если символа нет в словаре
                    {
                        stringBuilder.Append("*");
                    }
                }
                convertedWords.Add(stringBuilder.ToString().ToLower());
                stringBuilder.Clear();
            }
            return string.Join(" ", convertedWords);
        }
    }
}
