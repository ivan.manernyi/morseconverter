﻿using NUnit.Framework;
using MorseConverter;

namespace MorseConverterTests
{
    public class Tests
    {
        [Test]
        public void TestCorrectInputText_TwoWordsSeparatedByThreeSpaces() // Ожидаемый результат "hello world"
        {
            Assert.That(MorseConverter.Program.GetInputText(".... . .-.. .-.. ---   .-- --- .-. .-.. -.."), Is.EqualTo("hello world"));
        }

        [Test]
        public void TestCorrectInputText_OneCode() // Ожидаемый результат "h"
        {
            Assert.That(MorseConverter.Program.GetInputText("...."), Is.EqualTo("h"));
        }

        [Test]
        public void TestCorrectInputText_TwoCodesWithTwoSpacesBeforeAndAfter() // Ожидаемый результат "hh"
        {
            Assert.That(MorseConverter.Program.GetInputText("  ....  ....  "), Is.EqualTo("hh"));
        }

        [Test]
        public void TestCorrectInputText_AllAlphabetLettersCodesSeparatedByThreeSpaces() // Ожидаемый результат "a b c d e f g h i j k l m n o p q r s t u v w x y z"
        {
            Assert.That(MorseConverter.Program.GetInputText(".-   -...   -.-.   -..   .   ..-.   --.   ....   ..   .---   -.-   .-..   --   -.   ---   .--.   --.-   .-.   ...   -   ..-   ...-   .--   -..-   -.--   --.."), Is.EqualTo("a b c d e f g h i j k l m n o p q r s t u v w x y z"));
        }

        [Test]
        public void TestIncorrectInputText_TwoCodesWithoutSpacesLikeOneCode() // Ожидаемый результат "*"
        {
            Assert.That(MorseConverter.Program.GetInputText("........"), Is.EqualTo("*"));
        }

        [Test]
        public void TestIncorrectInputText_AllASCIIPunctuationAndEmoji() // Ожидаемый результат "*"
        {
            Assert.That(MorseConverter.Program.GetInputText(@",./;'[]\-=<>?:{}|+!@#$%^&*()`~\""" + @"😏"), Is.EqualTo("*"));
        }

        [Test]
        public void TestIncorrectInputText_SymbolsDoesntExistInDictionary() // Ожидаемый результат "abc ***"
        {
            Assert.That(MorseConverter.Program.GetInputText(".- -... -.-.   .---- ..--- ...--"), Is.EqualTo("abc ***"));
        }

        [Test]
        public void TestIncorrectInputText_NumericalValues() // Ожидаемый результат "***"
        {
            Assert.That(MorseConverter.Program.GetInputText("1 2 3 4 5 6 7 8 9 0"), Is.EqualTo("**********"));
        }

        [Test]
        public void TestIncorrectInputText_NullOrEmptyString() // Ожидаемый результат "NullOrEmptyStringError"
        {
            Assert.That(MorseConverter.Program.GetInputText(null), Is.EqualTo("NullOrEmptyStringError"));
        }

        [Test]
        public void TestIncorrectInputText_OnlySpacesString() // Ожидаемый результат "NullOrEmptyStringError"
        {
            Assert.That(MorseConverter.Program.GetInputText("          "), Is.EqualTo("NullOrEmptyStringError"));
        }
    }
}